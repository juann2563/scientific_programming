//Esta aplicacion obtiene el modelo en espacio de estados de un convertidor DC/DC concectado a un panel solar
// se deben poseer los párametros del fabricante del panel solar y de los dipositivos que componen el
// convertidor DC/DC
#include <iostream>

using namespace std;
// se crea la  clase convertidor
class Converter{
public:
    // constructor de la clase con los parámetros necesarios para el convertidor
    Converter(float,float,float,float,float,float,float,float,float,float,float,float,float);
    //Método que muestra la ecuaciones no linealizadas
    void ecuations();
    void balnce_charge();
    void stateSpace();
    void imprimir(float[]);
    void imprimir(float[3][3]);
public:
    // parámetros son:
    // Ci: valor capacitor de entrada en faradios(F)
    // Co: capacitor de salida
    // Vci: voltaje del capacitor de entrada
    // Vco: voltaje del capacitor de salida
    // I: corriente de la bobina
    // D: duty
    // VB: voltaje de la batería
    // Ron: pérdidas en el mosfet
    // Isc: corriente de cortocircuito del panel
    // A,B,C,D1 son las matrices para representar el modelo en espacio de estados
    // Rl: pérdidas en la bobina
    float Vci,Vco,I,RB,VB,Isc,Rmpp,Ci, L,Co,D,Ron,Rl, A[3][3], B[3][3], C[3], D1[3];
};

int main()
{
    // creación del objeto
    Converter convertidor(1.0,1.0,1.0,1.0,1.0,1.0,5.0,1.0,1.0,1.0,1.0,1.0,1.0);
    // implementacion de los métodos
    convertidor.ecuations();
    convertidor.print(convertidor.A); // se accede al valor de la matriz A que es un atributo
    convertidor.print(convertidor.B);
    convertidor.print(convertidor.C);
    convertidor.print(convertidor.D1);
    //convertidor.print(convertidor.C);
    return 0;
}
// -método constructor para asignar los parámetros a los atributos privados
Converter::Converter(float vci,float rb,float vco, float l,float i,float vb,float isc,float rmpp, float ci, float co,float d,float ron, float rl){
    Vci = vci;
    Vco = vco;
    I = i;
    VB = vb;
    Isc = isc;
    Rmpp = rmpp;
    Ci = ci;
    Co = co;
    D=d;
    Ron = ron;
    Rl = rl;
    L = l;
    RB = rb;
}
void Converter::ecuations(){
    cout << "When Q1 is ON and Q2 is off" << endl;
    cout << "Ici = Isc - Vci - I" <<endl;
    cout << "            ---" <<endl;
    cout << "            Rmpp"<<endl<<endl;
    cout << "Ico = -Vco + VB" <<endl;
    cout << "       ----  ---" <<endl;
    cout << "       RB    RB"<<endl<<endl;
    cout << "Ici = Vci - I(Rl + Ron)" <<endl<<endl;
}
void Converter::balnce_charge(){
    cout << "When Q1 is ON and Q2 is off" << endl;
    cout << "Ici = Isc - Vci - I" <<endl;
    cout << "            ---" <<endl;
    cout << "            Rmpp"<<endl<<endl;
    cout << "Ico = -Vco + VB" <<endl;
    cout << "       ----  ---" <<endl;
    cout << "       RB    RB"<<endl<<endl;
    cout << "Ici = Vci - I(Rl + Ron)" <<endl<<endl;
}

void Converter::stateSpace(){
    // State space matrix
    A[0][0] = -1/(Ci*Rmpp);
    A[0][1] = -1/Ci;
    A[0][2] = 0;
    A[1][0] = 1/L;
    A[1][1] = -(Rl+Ron)/L;
    A[1][2] = (D-1)/L;
    A[2][0] = 0;
    A[2][1] = (1-D)/Co;
    A[2][2] = -1/(Co*RB);

    B[0][0] = 1/Ci;
    B[0][1] = 0;
    B[0][2] = 0;
    B[1][0] = 0;
    B[1][1] = 0;
    B[1][2] = Vco/L;
    B[2][0] = 0;
    B[2][1] = 1/(Co*RB);
    B[2][2] = -I/Co;
    // vector de salida
    C[0] = 1;
    C[1] = 0;
    C[2] = 0;

    D1[0] = 0;
    D1[1] = 0;
    D1[2] = 0;

}
// print matirx
void Converter::imprimir(float m[][3]){

    for(int i=0; i<3; i++){
        cout << "|     ";
        for(int j=0; j<3; j++){
            cout << m[i][j] << "       ";
        }
        cout << " |" << endl;
    }
    cout << endl;
}
// print array
void Converter::imprimir(float m1[]){
    cout << "[      " ;
    for(int i=0; i<3; i++){
        cout << m1[i];
    }
    cout << "      ]" << endl;
}
